FROM java:8u40-jre

MAINTAINER Mayank Bairagi, mayank@knoldus.com

RUN echo "CREATING BUILD"

Add target/scala-2.11/codeship-docker-tutum-assembly-1.1.jar /app/server.jar

RUN echo "Starting App....."

ENV RUN_MODE STAGING

ENTRYPOINT ["java", "-jar", "/app/server.jar"]

