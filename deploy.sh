
######## prerequisite on Docker machine ######
#1) Docker Engine
#2) TUTUM
#3) SET TUTUM 
#Your login credentials will be stored in ~/.tutum:
#[auth]
#user = "username"
#apikey = "apikey"

##############################################

DOCKER_SERVER=root@54.148.161.217
BUILD_SOURCE=target/scala-2.11/codeship-docker-tutum-assembly-1.1.jar
DOCKER_FILE_SOURCE=Dockerfile
BUILD_TARGET_DIR=/mnt/deploy/docker-demo/target/scala-2.11
DOCKER_FILE_TARGET_DIR=/mnt/deploy/docker-demo

TUTUM_USER=mayankbairagi
TUTUM_API_KEY=7bd9c2a9d5993cdaf2296c3f99283d653e11af38
TUTUM_REPOSITORY=tutum.co/mayankbairagi/demo
IMAGE_NAME=demo
TAG_NAME=v3
TRIGGER_URL=https://dashboard.tutum.co/api/v1/service/3eb62fa2-b0da-49ff-b941-13b7dad6c618/trigger/0f34b862-f84d-499f-96a1-456ff9b2587f/call/



# create folder structure on remote host (prevent exceptions...)
 ssh $DOCKER_SERVER "mkdir -p $BUILD_TARGET_DIR" && \
 scp -rp $BUILD_SOURCE  $DOCKER_SERVER:$BUILD_TARGET_DIR  && \
 scp -rp $DOCKER_FILE_SOURCE  $DOCKER_SERVER:$DOCKER_FILE_TARGET_DIR && \
 ssh $DOCKER_SERVER  docker build -t $IMAGE_NAME:$TAG_NAME $DOCKER_FILE_TARGET_DIR && \
 ssh $DOCKER_SERVER docker tag -f $IMAGE_NAME:$TAG_NAME $TUTUM_REPOSITORY:$TAG_NAME
 ssh $DOCKER_SERVER "export TUTUM_USER=$TUTUM_USER && export TUTUM_APIKEY=$TUTUM_API_KEY && tutum push $TUTUM_REPOSITORY:$TAG_NAME"
 curl -XPOST $TRIGGER_URL
 ssh $DOCKER_SERVER rm -fr $DOCKER_FILE_TARGET_DIR
 

ssh $DOCKER_SERVER  docker build -t demo:v3 $DOCKER_FILE_TARGET_DIR
