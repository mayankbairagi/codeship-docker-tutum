import AssemblyKeys._ // put this at the top of the file

name := "codeship-docker-tutum"

organization := "mayankbairagi"

scalaVersion:="2.11.4"

version:="1.1"

assemblySettings

libraryDependencies ++= {
  	Seq(
		"org.scalatest"                 % "scalatest_2.11"        % "2.2.4",
		"ch.qos.logback"                % "logback-classic"       % "1.1.2"
)
	}

